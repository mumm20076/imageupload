const http = require("http");
const path = require("path");
const fs = require("fs");

const bodyParser = require("body-parser");

const express = require("express");

const app = express();
const httpServer = http.createServer(app);

const HOSTNAME = "172.20.10.9"
const PORT = 3000;

httpServer.listen(PORT, HOSTNAME, () => {
  console.log(`Server is listening on http://${HOSTNAME}:${PORT}`);
});

app.use("/", express.static('./public/'));
app.get("/", express.static(path.join(__dirname, "./public")));

const handleError = (err, res) => {
  res
    .status(500)
    .contentType("text/plain")
    .end("Oops! Something went wrong!");
};

app.post("/upload",
  bodyParser.raw({type: ["image/jpeg", "image/png"], limit: "10mb"}),
  (req, res) => {
    if(!req.body) {
        console.log('Body is nil');
    }
    else {
        console.log('Body is Present');
    }
    try {
      const filePath = "uploads/" + Date.now() + ".jpeg";
      console.log(req.body);
      fs.writeFile(filePath, req.body, (error) => {
        if (error) {
          throw error;
        }
      });

      res.sendStatus(200);
    } catch (error) {
      res.sendStatus(500);
    }
  });
